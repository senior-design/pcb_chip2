/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

uint8_t message = 0;
// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************
static void ACK1_User_Handler(GPIO_PIN pin, uintptr_t context)
{
    if (ACK1_Get() == 1){
        ACK2_Set();
        message = (F42_Get()<<7) | (F41_Get()<<6) | (F16_Get()<<5) | (F15_Get()<<4) | (F14_Get()<<3) | (F13_Get()<<2) | (F12_Get()<<1) | F11_Get();
        ACK2_Clear();
    }
}

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    GPIO_PinInterruptCallbackRegister(ACK1_PIN , ACK1_User_Handler, 0);
    GPIO_PinInterruptEnable(ACK1_PIN);
    ACK2_Clear();
    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

