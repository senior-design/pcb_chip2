/*******************************************************************************
  GPIO PLIB

  Company:
    Microchip Technology Inc.

  File Name:
    plib_gpio.h

  Summary:
    GPIO PLIB Header File

  Description:
    This library provides an interface to control and interact with Parallel
    Input/Output controller (GPIO) module.

*******************************************************************************/

/*******************************************************************************
* Copyright (C) 2019 Microchip Technology Inc. and its subsidiaries.
*
* Subject to your compliance with these terms, you may use Microchip software
* and any derivatives exclusively with Microchip products. It is your
* responsibility to comply with third party license terms applicable to your
* use of third party software (including open source software) that may
* accompany Microchip software.
*
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
* EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
* WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
* PARTICULAR PURPOSE.
*
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
* FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
* ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
* THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
*******************************************************************************/

#ifndef PLIB_GPIO_H
#define PLIB_GPIO_H

#include <device.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

    extern "C" {

#endif
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Data types and constants
// *****************************************************************************
// *****************************************************************************


/*** Macros for F42 pin ***/
#define F42_Set()               (LATGSET = (1<<8))
#define F42_Clear()             (LATGCLR = (1<<8))
#define F42_Toggle()            (LATGINV= (1<<8))
#define F42_OutputEnable()      (TRISGCLR = (1<<8))
#define F42_InputEnable()       (TRISGSET = (1<<8))
#define F42_Get()               ((PORTG >> 8) & 0x1)
#define F42_PIN                  GPIO_PIN_RG8

/*** Macros for F41 pin ***/
#define F41_Set()               (LATGSET = (1<<9))
#define F41_Clear()             (LATGCLR = (1<<9))
#define F41_Toggle()            (LATGINV= (1<<9))
#define F41_OutputEnable()      (TRISGCLR = (1<<9))
#define F41_InputEnable()       (TRISGSET = (1<<9))
#define F41_Get()               ((PORTG >> 9) & 0x1)
#define F41_PIN                  GPIO_PIN_RG9

/*** Macros for F16 pin ***/
#define F16_Set()               (LATESET = (1<<12))
#define F16_Clear()             (LATECLR = (1<<12))
#define F16_Toggle()            (LATEINV= (1<<12))
#define F16_OutputEnable()      (TRISECLR = (1<<12))
#define F16_InputEnable()       (TRISESET = (1<<12))
#define F16_Get()               ((PORTE >> 12) & 0x1)
#define F16_PIN                  GPIO_PIN_RE12

/*** Macros for F15 pin ***/
#define F15_Set()               (LATESET = (1<<13))
#define F15_Clear()             (LATECLR = (1<<13))
#define F15_Toggle()            (LATEINV= (1<<13))
#define F15_OutputEnable()      (TRISECLR = (1<<13))
#define F15_InputEnable()       (TRISESET = (1<<13))
#define F15_Get()               ((PORTE >> 13) & 0x1)
#define F15_PIN                  GPIO_PIN_RE13

/*** Macros for F14 pin ***/
#define F14_Set()               (LATESET = (1<<14))
#define F14_Clear()             (LATECLR = (1<<14))
#define F14_Toggle()            (LATEINV= (1<<14))
#define F14_OutputEnable()      (TRISECLR = (1<<14))
#define F14_InputEnable()       (TRISESET = (1<<14))
#define F14_Get()               ((PORTE >> 14) & 0x1)
#define F14_PIN                  GPIO_PIN_RE14

/*** Macros for F13 pin ***/
#define F13_Set()               (LATESET = (1<<15))
#define F13_Clear()             (LATECLR = (1<<15))
#define F13_Toggle()            (LATEINV= (1<<15))
#define F13_OutputEnable()      (TRISECLR = (1<<15))
#define F13_InputEnable()       (TRISESET = (1<<15))
#define F13_Get()               ((PORTE >> 15) & 0x1)
#define F13_PIN                  GPIO_PIN_RE15

/*** Macros for F12 pin ***/
#define F12_Set()               (LATBSET = (1<<4))
#define F12_Clear()             (LATBCLR = (1<<4))
#define F12_Toggle()            (LATBINV= (1<<4))
#define F12_OutputEnable()      (TRISBCLR = (1<<4))
#define F12_InputEnable()       (TRISBSET = (1<<4))
#define F12_Get()               ((PORTB >> 4) & 0x1)
#define F12_PIN                  GPIO_PIN_RB4

/*** Macros for F11 pin ***/
#define F11_Set()               (LATASET = (1<<4))
#define F11_Clear()             (LATACLR = (1<<4))
#define F11_Toggle()            (LATAINV= (1<<4))
#define F11_OutputEnable()      (TRISACLR = (1<<4))
#define F11_InputEnable()       (TRISASET = (1<<4))
#define F11_Get()               ((PORTA >> 4) & 0x1)
#define F11_PIN                  GPIO_PIN_RA4

/*** Macros for PDM_Out pin ***/
#define PDM_Out_Set()               (LATFSET = (1<<1))
#define PDM_Out_Clear()             (LATFCLR = (1<<1))
#define PDM_Out_Toggle()            (LATFINV= (1<<1))
#define PDM_Out_OutputEnable()      (TRISFCLR = (1<<1))
#define PDM_Out_InputEnable()       (TRISFSET = (1<<1))
#define PDM_Out_Get()               ((PORTF >> 1) & 0x1)
#define PDM_Out_PIN                  GPIO_PIN_RF1

/*** Macros for ACK2 pin ***/
#define ACK2_Set()               (LATBSET = (1<<12))
#define ACK2_Clear()             (LATBCLR = (1<<12))
#define ACK2_Toggle()            (LATBINV= (1<<12))
#define ACK2_OutputEnable()      (TRISBCLR = (1<<12))
#define ACK2_InputEnable()       (TRISBSET = (1<<12))
#define ACK2_Get()               ((PORTB >> 12) & 0x1)
#define ACK2_PIN                  GPIO_PIN_RB12

/*** Macros for ACK1 pin ***/
#define ACK1_Set()               (LATBSET = (1<<13))
#define ACK1_Clear()             (LATBCLR = (1<<13))
#define ACK1_Toggle()            (LATBINV= (1<<13))
#define ACK1_OutputEnable()      (TRISBCLR = (1<<13))
#define ACK1_InputEnable()       (TRISBSET = (1<<13))
#define ACK1_Get()               ((PORTB >> 13) & 0x1)
#define ACK1_PIN                  GPIO_PIN_RB13
#define ACK1_InterruptEnable()   (CNENBSET = (1<<13))
#define ACK1_InterruptDisable()  (CNENBCLR = (1<<13))


// *****************************************************************************
/* GPIO Port

  Summary:
    Identifies the available GPIO Ports.

  Description:
    This enumeration identifies the available GPIO Ports.

  Remarks:
    The caller should not rely on the specific numbers assigned to any of
    these values as they may change from one processor to the next.

    Not all ports are available on all devices.  Refer to the specific
    device data sheet to determine which ports are supported.
*/

typedef enum
{
    GPIO_PORT_A = 0,
    GPIO_PORT_B = 1,
    GPIO_PORT_C = 2,
    GPIO_PORT_D = 3,
    GPIO_PORT_E = 4,
    GPIO_PORT_F = 5,
    GPIO_PORT_G = 6,
} GPIO_PORT;

typedef enum
{
    GPIO_INTERRUPT_ON_MISMATCH,
    GPIO_INTERRUPT_ON_RISING_EDGE,
    GPIO_INTERRUPT_ON_FALLING_EDGE,
    GPIO_INTERRUPT_ON_BOTH_EDGES,
}GPIO_INTERRUPT_STYLE;

// *****************************************************************************
/* GPIO Port Pins

  Summary:
    Identifies the available GPIO port pins.

  Description:
    This enumeration identifies the available GPIO port pins.

  Remarks:
    The caller should not rely on the specific numbers assigned to any of
    these values as they may change from one processor to the next.

    Not all pins are available on all devices.  Refer to the specific
    device data sheet to determine which pins are supported.
*/

typedef enum
{
    GPIO_PIN_RA0 = 0,
    GPIO_PIN_RA1 = 1,
    GPIO_PIN_RA4 = 4,
    GPIO_PIN_RA7 = 7,
    GPIO_PIN_RA8 = 8,
    GPIO_PIN_RA10 = 10,
    GPIO_PIN_RA11 = 11,
    GPIO_PIN_RA12 = 12,
    GPIO_PIN_RB0 = 16,
    GPIO_PIN_RB1 = 17,
    GPIO_PIN_RB2 = 18,
    GPIO_PIN_RB3 = 19,
    GPIO_PIN_RB4 = 20,
    GPIO_PIN_RB5 = 21,
    GPIO_PIN_RB6 = 22,
    GPIO_PIN_RB7 = 23,
    GPIO_PIN_RB8 = 24,
    GPIO_PIN_RB9 = 25,
    GPIO_PIN_RB10 = 26,
    GPIO_PIN_RB11 = 27,
    GPIO_PIN_RB12 = 28,
    GPIO_PIN_RB13 = 29,
    GPIO_PIN_RB14 = 30,
    GPIO_PIN_RB15 = 31,
    GPIO_PIN_RC0 = 32,
    GPIO_PIN_RC1 = 33,
    GPIO_PIN_RC2 = 34,
    GPIO_PIN_RC6 = 38,
    GPIO_PIN_RC7 = 39,
    GPIO_PIN_RC8 = 40,
    GPIO_PIN_RC9 = 41,
    GPIO_PIN_RC10 = 42,
    GPIO_PIN_RC11 = 43,
    GPIO_PIN_RC12 = 44,
    GPIO_PIN_RC13 = 45,
    GPIO_PIN_RC15 = 47,
    GPIO_PIN_RD5 = 53,
    GPIO_PIN_RD6 = 54,
    GPIO_PIN_RE12 = 76,
    GPIO_PIN_RE13 = 77,
    GPIO_PIN_RE14 = 78,
    GPIO_PIN_RE15 = 79,
    GPIO_PIN_RF0 = 80,
    GPIO_PIN_RF1 = 81,
    GPIO_PIN_RG6 = 102,
    GPIO_PIN_RG7 = 103,
    GPIO_PIN_RG8 = 104,
    GPIO_PIN_RG9 = 105,

    /* This element should not be used in any of the GPIO APIs.
       It will be used by other modules or application to denote that none of the GPIO Pin is used */
    GPIO_PIN_NONE = -1

} GPIO_PIN;

typedef  void (*GPIO_PIN_CALLBACK) ( GPIO_PIN pin, uintptr_t context);

void GPIO_Initialize(void);

// *****************************************************************************
// *****************************************************************************
// Section: GPIO Functions which operates on multiple pins of a port
// *****************************************************************************
// *****************************************************************************

uint32_t GPIO_PortRead(GPIO_PORT port);

void GPIO_PortWrite(GPIO_PORT port, uint32_t mask, uint32_t value);

uint32_t GPIO_PortLatchRead ( GPIO_PORT port );

void GPIO_PortSet(GPIO_PORT port, uint32_t mask);

void GPIO_PortClear(GPIO_PORT port, uint32_t mask);

void GPIO_PortToggle(GPIO_PORT port, uint32_t mask);

void GPIO_PortInputEnable(GPIO_PORT port, uint32_t mask);

void GPIO_PortOutputEnable(GPIO_PORT port, uint32_t mask);

void GPIO_PortInterruptEnable(GPIO_PORT port, uint32_t mask);

void GPIO_PortInterruptDisable(GPIO_PORT port, uint32_t mask);

// *****************************************************************************
// *****************************************************************************
// Section: Local Data types and Prototypes
// *****************************************************************************
// *****************************************************************************

typedef struct {

    /* target pin */
    GPIO_PIN                 pin;

    /* Callback for event on target pin*/
    GPIO_PIN_CALLBACK        callback;

    /* Callback Context */
    uintptr_t               context;

} GPIO_PIN_CALLBACK_OBJ;

// *****************************************************************************
// *****************************************************************************
// Section: GPIO Functions which operates on one pin at a time
// *****************************************************************************
// *****************************************************************************

static inline void GPIO_PinWrite(GPIO_PIN pin, bool value)
{
    GPIO_PortWrite((GPIO_PORT)(pin>>4), (uint32_t)(0x1) << (pin & 0xF), (uint32_t)(value) << (pin & 0xF));
}

static inline bool GPIO_PinRead(GPIO_PIN pin)
{
    return (bool)(((GPIO_PortRead((GPIO_PORT)(pin>>4))) >> (pin & 0xF)) & 0x1);
}

static inline bool GPIO_PinLatchRead(GPIO_PIN pin)
{
    return (bool)((GPIO_PortLatchRead((GPIO_PORT)(pin>>4)) >> (pin & 0xF)) & 0x1);
}

static inline void GPIO_PinToggle(GPIO_PIN pin)
{
    GPIO_PortToggle((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinSet(GPIO_PIN pin)
{
    GPIO_PortSet((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinClear(GPIO_PIN pin)
{
    GPIO_PortClear((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinInputEnable(GPIO_PIN pin)
{
    GPIO_PortInputEnable((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinOutputEnable(GPIO_PIN pin)
{
    GPIO_PortOutputEnable((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

#define GPIO_PinInterruptEnable(pin)       GPIO_PinIntEnable(pin, GPIO_INTERRUPT_ON_MISMATCH)
#define GPIO_PinInterruptDisable(pin)      GPIO_PinIntDisable(pin)

void GPIO_PinIntEnable(GPIO_PIN pin, GPIO_INTERRUPT_STYLE style);
void GPIO_PinIntDisable(GPIO_PIN pin);

bool GPIO_PinInterruptCallbackRegister(
    GPIO_PIN pin,
    const   GPIO_PIN_CALLBACK callBack,
    uintptr_t context
);

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

    }

#endif
// DOM-IGNORE-END
#endif // PLIB_GPIO_H
